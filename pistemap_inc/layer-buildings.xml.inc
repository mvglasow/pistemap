<Style name="building">
	<Rule>
		&maxscale_zoom13;
		<PolygonSymbolizer fill-opacity=".5" fill="#907d60"/>
	</Rule>
	<Rule>
	        &maxscale_zoom17;
 		<LineSymbolizer stroke="#907d60" stroke-width="0.7" stroke-opacity="0.5"/>
	</Rule>
</Style>
<Layer name="building" status="on" srs="&osm2pgsql_projection;">
	<StyleName>building</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,building,leisure,railway,amenity,aeroway from &prefix;_polygon
       where building is not null
          or railway='station'
          or amenity='place_of_worship'
          or aeroway='terminal'
       order by z_order,way_area desc) as buildings
      </Parameter>
      &datasource-settings;
	</Datasource>
</Layer>
