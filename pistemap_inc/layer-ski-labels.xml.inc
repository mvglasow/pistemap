<Style name="piste-text">
    <Rule>
      &maxscale_zoom14;
      <Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'novice' and [ref] &lt;&gt; ''</Filter>
      <ShieldSymbolizer file="pistemap_symbols/piste-novice.16.png" fontset-name="book-fonts" size="8" fill="#fff" placement="line">[ref]</ShieldSymbolizer>
    </Rule>
    <Rule>
      &maxscale_zoom14;
      <Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'easy' and [ref] &lt;&gt; ''</Filter>
      <ShieldSymbolizer file="pistemap_symbols/piste-easy.16.png" fontset-name="book-fonts" size="8" fill="#fff" placement="line">[ref]</ShieldSymbolizer>
    </Rule>
    <Rule>
      &maxscale_zoom14;
      <Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'intermediate' and [ref] &lt;&gt; ''</Filter>
      <ShieldSymbolizer file="pistemap_symbols/piste-intermediate.16.png" fontset-name="book-fonts" size="8" fill="#fff" placement="line">[ref]</ShieldSymbolizer>
    </Rule>
    <Rule>
      &maxscale_zoom14;
      <Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'advanced' and [ref] &lt;&gt; ''</Filter>
      <ShieldSymbolizer file="pistemap_symbols/piste-advanced.16.png" fontset-name="book-fonts" size="8" fill="#fff" placement="line">[ref]</ShieldSymbolizer>
    </Rule>
    <Rule>
      &maxscale_zoom14;
      <Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'expert' and [ref] &lt;&gt; ''</Filter>
      <ShieldSymbolizer file="pistemap_symbols/piste-expert.16.png" fontset-name="book-fonts" size="8" fill="#fff" placement="line">[ref]</ShieldSymbolizer>
    </Rule>
    <Rule>
      &maxscale_zoom14;
      <Filter>[piste:type] = 'downhill' and [name] &lt;&gt; ''</Filter>
      <TextSymbolizer fontset-name="book-fonts" size="9" fill="#000" halo-radius="1" placement="line" dy="6">[name]</TextSymbolizer>
    </Rule>
    <Rule>
      &maxscale_zoom14;
      <Filter>[piste:type] = 'nordic' and [name] &lt;&gt; ''</Filter>
      <TextSymbolizer fontset-name="book-fonts" size="9" fill="#000" halo-radius="1" placement="line" dy="6">[name]</TextSymbolizer>
    </Rule>
</Style>

<Style name="piste-arrows">
    <Rule>
      <Filter>([piste:type] = 'downhill') and ([piste:difficulty] = 'novice') and ([oneway] = 'yes' or not [oneway] &lt;&gt; '')</Filter>
      &maxscale_zoom14;
      <MarkersSymbolizer fill="#393" stroke="#ffffff" />
    </Rule>
    <Rule>
      <Filter>([piste:type] = 'downhill') and ([piste:difficulty] = 'easy') and ([oneway] = 'yes' or not [oneway] &lt;&gt; '')</Filter>
      &maxscale_zoom14;
      <MarkersSymbolizer fill="#069" stroke="#ffffff" />
    </Rule>
    <Rule>
      <Filter>([piste:type] = 'downhill') and ([piste:difficulty] = 'intermediate') and ([oneway] = 'yes' or not [oneway] &lt;&gt; '')</Filter>
      &maxscale_zoom14;
      <MarkersSymbolizer fill="#f00" stroke="#ffffff" />
    </Rule>
    <Rule>
      <Filter>([piste:type] = 'downhill') and ([piste:difficulty] = 'advanced') and ([oneway] = 'yes' or not [oneway] &lt;&gt; '')</Filter>
      &maxscale_zoom14;
      <MarkersSymbolizer fill="#000000" stroke="#ffffff" />
    </Rule>
    <Rule>
      <Filter>([piste:type] = 'downhill') and ([piste:difficulty] = 'expert') and ([oneway] = 'yes' or not [oneway] &lt;&gt; '')</Filter>
      &maxscale_zoom14;
      <MarkersSymbolizer fill="#f90" stroke="#ffffff" />
    </Rule>
    <Rule>
      <Filter>([piste:type] = 'downhill') and ([piste:difficulty] = 'freeride') and ([oneway] = 'yes' or not [oneway] &lt;&gt; '')</Filter>
      &maxscale_zoom14;
      <MarkersSymbolizer fill="#000000" stroke="#ffffff" />
    </Rule>
</Style>

<Style name="lift-text">
    <Rule>
      &maxscale_zoom14;
      <Filter>[aerialway] &lt;&gt; ''</Filter>
      <TextSymbolizer fontset-name="book-fonts" size="9" fill="#000" halo-radius="1" placement="line" dy="7">[name]</TextSymbolizer>
    </Rule>
</Style>

<Style name="lift-icons">
    <Rule>
      <Filter>[aerialway] = 'drag_lift' or [aerialway] = 't-bar' or [aerialway] = 'j-bar' or [aerialway] = 'platter'</Filter>
      &maxscale_zoom12;
      <PointSymbolizer file="pistemap_symbols/drag_lift.16.png" />
    </Rule>
    <Rule>
      <Filter>[aerialway] = 'rope_tow'</Filter>
      &maxscale_zoom12;
      <PointSymbolizer file="pistemap_symbols/rope_tow.16.png" />
    </Rule>
    <Rule>
      <Filter>[aerialway] = 'magic_carpet'</Filter>
      &maxscale_zoom12;
      <PointSymbolizer file="pistemap_symbols/magic_carpet.16.png" />
    </Rule>
    <Rule>
      <Filter>[aerialway] = 'chair_lift'</Filter>
      &maxscale_zoom12;
      <PointSymbolizer file="pistemap_symbols/chair_lift.16.png" />
    </Rule>
    <Rule>
      <Filter>[aerialway] = 'mixed_lift'</Filter>
      &maxscale_zoom12;
      <PointSymbolizer file="pistemap_symbols/mixed_lift.16.png" />
    </Rule>
    <Rule>
      <Filter>[aerialway] = 'gondola'</Filter>
      &maxscale_zoom12;
      <PointSymbolizer file="pistemap_symbols/gondola.16.png" />
    </Rule>
    <Rule>
      <Filter>[aerialway] = 'cable_car'</Filter>
      &maxscale_zoom12;
      <PointSymbolizer file="pistemap_symbols/cable_car.16.png" />
    </Rule>
    <Rule>
       <Filter>[aerialway] &lt;&gt; ''</Filter>
      &maxscale_zoom15;
      <MarkersSymbolizer fill="#000000" stroke="#ffffff" />
    </Rule>
</Style>


  <Layer name="piste-labels" status="on">
    <StyleName>piste-text</StyleName>
    <StyleName>piste-arrows</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way, highway, oneway, coalesce("piste:name", case when highway is null then name else null end) as name, coalesce("piste:ref", case when highway is null then ref else null end) as ref, "piste:type", "piste:difficulty"
       from &prefix;_line
       where ("piste:type" is not null)
       order by z_order
      ) as objects
		</Parameter>
	&datasource-settings;
	</Datasource>
   </Layer>

  <Layer name="lift-labels" status="on">
    <StyleName>lift-icons</StyleName>
    <StyleName>lift-text</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way, highway, coalesce(aerialway, "piste:lift") as aerialway, oneway, coalesce(name, case when "piste:lift" is not null then "piste:name" else null end) as name, coalesce(ref, case when "piste:lift" is not null then "piste:ref" else null end) as ref
       from &prefix;_line
       where (aerialway is not null) or ("piste:lift" is not null)
       order by z_order
      ) as objects
		</Parameter>
	&datasource-settings;
	</Datasource>
   </Layer>

