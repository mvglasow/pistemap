<Style name="resorts">
	<Rule>
		&minscale_zoom9;
		<Filter>[landuse] = 'winter_sports'</Filter>
		<PolygonSymbolizer>
			<CssParameter name="fill">#d5e6ee</CssParameter>
		</PolygonSymbolizer>
		<LineSymbolizer stroke="#aaccdd" stroke-width="1.5" stroke-linejoin="round" stroke-linecap="round" />
	</Rule>
    <Rule>
		&minscale_zoom8;
		<Filter>[landuse] = 'winter_sports'</Filter>
      <TextSymbolizer fontset-name="book-fonts" size="9" fill="#000" halo-radius="1" placement="point">[name]</TextSymbolizer>
    </Rule>
</Style>

<Style name="lift-stations">
	<Rule>
      &maxscale_zoom9;
      <PointSymbolizer file="pistemap_symbols/aerialway-station.4.png" allow-overlap="yes" />
    </Rule>
</Style>

<Style name="lifts">
	<Rule>
		<Filter>[piste:lift] = 'drag_lift' or [aerialway] = 'drag_lift' or [piste:lift] = 't-bar' or [aerialway] = 't-bar' or [piste:lift] = 'j-bar' or [aerialway] = 'j-bar' or [piste:lift] = 'platter' or [aerialway] = 'platter' or [piste:lift] = 'rope_tow' or [aerialway] = 'rope_tow' or [piste:lift] = 'magic_carpet' or [aerialway] = 'magic_carpet' or [aerialway] = 'cable_car' or [aerialway] = 'gondola' or [aerialway] = 'chair_lift' or [aerialway] = 'mixed_lift'</Filter>
		&maxscale_zoom9;
		<LineSymbolizer stroke="#000" stroke-width="2" stroke-linejoin="round" stroke-linecap="round" />
	</Rule>
</Style>

<Style name="pistes-sled">
	<Rule>
		<Filter>[piste:type] = 'sled'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#80080" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" stroke-dasharray="1,2" />
	</Rule>
</Style>

<Style name="pistes-nordic">
	<Rule>
		<Filter>[piste:type] = 'nordic' and not [piste:difficulty] &lt;&gt; ''</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#550000" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" stroke-dasharray="10,5" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'nordic' and [piste:difficulty] = 'novice'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#393" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" stroke-dasharray="10,5" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'nordic' and [piste:difficulty] = 'easy'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#069" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" stroke-dasharray="10,5" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'nordic' and [piste:difficulty] = 'intermediate'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#f00" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" stroke-dasharray="10,5" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'nordic' and [piste:difficulty] = 'advanced'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#000" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" stroke-dasharray="10,5" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'nordic' and [piste:difficulty] = 'expert'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#f90" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" stroke-dasharray="10,5" />
	</Rule>
</Style>

<Style name="pistes-downhill">
	<Rule>
		<Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'novice'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#393" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'easy'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#069" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'intermediate'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#f00" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'advanced'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#000" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'expert'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#f90" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" />
	</Rule>
	<Rule>
		<Filter>[piste:type] = 'downhill' and [piste:difficulty] = 'freeride'</Filter>
		&maxscale_zoom11;
		<LineSymbolizer stroke="#f90" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" stroke-dasharray="0,7,7,0" />
		<LineSymbolizer stroke="#000" stroke-width="1" stroke-linejoin="round" stroke-linecap="round" stroke-dasharray="7,7" />
	</Rule>
</Style>

  <Layer name="resorts" status="on">
    <StyleName>resorts</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,landuse,name
       from &prefix;_polygon
       where (landuse = 'winter_sports')
       order by z_order
      ) as areas
		</Parameter>
	&datasource-settings;
	</Datasource>
   </Layer>
   
  <Layer name="pistes" status="on">
    <StyleName>pistes-sled</StyleName>
    <StyleName>pistes-nordic</StyleName>
    <StyleName>pistes-downhill</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,highway,"piste:type","piste:difficulty"
       from &prefix;_line
       where ("piste:type" is not null)
       order by z_order
      ) as pistes
		</Parameter>
	&datasource-settings;
	</Datasource>
   </Layer>

  <Layer name="lifts" status="on">
    <StyleName>lifts</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,highway,aerialway,"piste:lift"
       from &prefix;_line
       where (aerialway is not null) or ("piste:lift" is not null)
       order by z_order
      ) as lifts
		</Parameter>
	&datasource-settings;
	</Datasource>
   </Layer>
   
  <Layer name="lift-stations" status="on">
    <StyleName>lift-stations</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,highway,aerialway
       from &prefix;_point
       where (aerialway = 'station')
       order by z_order
      ) as lifts
		</Parameter>
	&datasource-settings;
	</Datasource>
   </Layer>

