<Style name="raster">
	<Rule>
		&maxscale_zoom6;
		<RasterSymbolizer scaling="bilinear" mode="multiple">
		   <RasterColorizer default-mode="linear" default-color="#008000" epsilon="2">
		      <stop color="#006080" value="0" mode="linear" />	
		      <stop color="#ffffff" value="256" mode="linear" />	
		   </RasterColorizer>
		</RasterSymbolizer>
	</Rule>
</Style>

<Layer name="dem-N45E006" status="on">
	<StyleName>raster</StyleName>
	<Datasource>
		<Parameter name="type">gdal</Parameter>
		<Parameter name="file">srtm/N45E006_hillshade.tif</Parameter>
		<Parameter name="format">tiff</Parameter>
		<Parameter name="band">1</Parameter>
	</Datasource>
</Layer>

<Layer name="dem-N47E010" status="on">
	<StyleName>raster</StyleName>
	<Datasource>
		<Parameter name="type">gdal</Parameter>
		<Parameter name="file">srtm/N47E010_hillshade.tif</Parameter>
		<Parameter name="format">tiff</Parameter>
		<Parameter name="band">1</Parameter>
	</Datasource>
</Layer>

