<!--pedestrian walkways-->
<Style name="pedestrian-walkways">
	<Rule>
		<Filter>([highway] = 'pedestrian' or [highway]='footway' or ([highway]='path' and !([bicycle]='designated')) or [highway]='track') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom15;
		&minscale_zoom16;
		<LineSymbolizer stroke="#907d60" stroke-width="1.5"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'pedestrian' or [highway]='footway' or ([highway]='path' and !([bicycle]='designated')) or [highway]='track') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom17;
		<LineSymbolizer stroke="#907d60" stroke-width="2"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'steps') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom15;
		&minscale_zoom16;
		<LineSymbolizer stroke="#907d60" stroke-width="3"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'steps') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom17;
		<LineSymbolizer stroke="#907d60" stroke-width="4"/>
	</Rule>
</Style>
<Style name="pedestrian-polygon">
	<Rule>
		<Filter>([highway] = 'living_street' or [highway] = 'pedestrian' or [highway]='service' or [highway]='footway' or [highway]='path' or [highway] = 'residential' or [highway]='unclassified' or [highway]='service' or [highway] = 'track') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		<PolygonSymbolizer fill="#907d60"/>
	</Rule>
</Style>
<!--cycleway-->
<Style name="cycleway">
	<Rule>
		<Filter>([highway] = 'cycleway' or ([highway] = 'path' and [bicycle] = 'designated') or [highway]='bridleway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom17;
		&minscale_zoom17;
		<LineSymbolizer stroke="#907d60" stroke-width="2.0" stroke-opacity="1" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'cycleway' or ([highway] = 'path' and [bicycle] = 'designated') or [highway]='bridleway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom18;
		<LineSymbolizer stroke="#907d60" stroke-width="3.5" stroke-opacity="1" stroke-linecap="round"/>
	</Rule>
</Style>
<!--runway-->
<Style name="runway">
	<Rule>
		<Filter>[aeroway] = 'runway'</Filter>
		&maxscale_zoom12;
		&maxscale_zoom12;
		<LineSymbolizer stroke="#b4b4b4" stroke-width="3"/>
	</Rule>
	<Rule>
		<Filter>[aeroway] = 'runway'</Filter>
		&maxscale_zoom13;
		&maxscale_zoom14;
		<LineSymbolizer stroke="#b4b4b4" stroke-width="5"/>
	</Rule>
	<Rule>
		<Filter>[aeroway] = 'runway'</Filter>
		&maxscale_zoom15;
		&maxscale_zoom16;
		<LineSymbolizer stroke="#b4b4b4" stroke-width="15"/>
	</Rule>
	<Rule>
		<Filter>[aeroway] = 'runway'</Filter>
		&maxscale_zoom17;
		<LineSymbolizer stroke="#b4b4b4" stroke-width="45"/>
	</Rule>
	<Rule>
		<Filter>[aeroway] = 'taxiway'</Filter>
		&maxscale_zoom12;
		&maxscale_zoom12;
		<LineSymbolizer stroke="#b4b4b4" stroke-width="1"/>
	</Rule>
	<Rule>
		<Filter>[aeroway] = 'taxiway'</Filter>
		&maxscale_zoom13;
		&maxscale_zoom14;
		<LineSymbolizer stroke="#b4b4b4" stroke-width="2"/>
	</Rule>
	<Rule>
		<Filter>[aeroway] = 'taxiway'</Filter>
		&maxscale_zoom15;
		&maxscale_zoom16;
		<LineSymbolizer stroke="#b4b4b4" stroke-width="5"/>
	</Rule>
	<Rule>
		<Filter>[aeroway] = 'taxiway'</Filter>
		&maxscale_zoom17;
		<LineSymbolizer stroke="#b4b4b4" stroke-width="10"/>
	</Rule>
</Style>
<!--rail-->
<Style name="rail">
	<Rule>
		<Filter>([railway] = 'rail' or [railway] = 'light_rail' or [railway] = 'subway' or [railway] = 'monorail') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom11;
		&minscale_zoom13;
		<LineSymbolizer stroke-linejoin="round" stroke="#a6a8aa" stroke-width="1" stroke-dasharray="1,1"/>
	</Rule>
	<Rule>
		<Filter>([railway] = 'rail' or [railway] = 'light_rail' or [railway] = 'subway' or [railway] = 'monorail') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		<LineSymbolizer stroke-linejoin="round" stroke="#afafaf" stroke-width="2.5"/>
	</Rule>
	<Rule>
		<Filter>([railway] = 'rail' or [railway] = 'light_rail' or [railway] = 'subway' or [railway] = 'monorail') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		<LineSymbolizer stroke-linejoin="round" stroke="#ffffff" stroke-width="1.5" stroke-dasharray="5,4"/>
	</Rule>
</Style>
<!--alley-->
<Style name="alley">
	<Rule>
		<Filter>([highway]='service') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		&minscale_zoom14;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width=".5" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway]='service') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom15;
		&minscale_zoom15;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway]='service') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom16;
		&minscale_zoom17;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="4" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway]='service') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom18;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="5" stroke-linecap="round"/>
	</Rule>
</Style>
<!--tertiary and residential-->
<Style name="tertiary">
	<Rule>
		<Filter>([highway] = 'tertiary' or [highway] = 'residential' or [highway] = 'living_street' or [highway] = 'unclassified' or [highway] = 'tertiary_link' or [highway] = 'residential_link' or [highway] = 'unclassified_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom12;
		&minscale_zoom12;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width=".5" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'tertiary' or [highway] = 'residential' or [highway] = 'living_street' or [highway] = 'unclassified' or [highway] = 'tertiary_link' or [highway] = 'residential_link' or [highway] = 'unclassified_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom13;
		&minscale_zoom13;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width=".65" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'tertiary' or [highway] = 'residential' or [highway] = 'living_street' or [highway] = 'unclassified' or [highway] = 'tertiary_link' or [highway] = 'residential_link' or [highway] = 'unclassified_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		&minscale_zoom14;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="2" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'tertiary' or [highway] = 'residential' or [highway] = 'living_street' or [highway] = 'unclassified' or [highway] = 'tertiary_link' or [highway] = 'residential_link' or [highway] = 'unclassified_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom15;
		&minscale_zoom15;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="3" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'tertiary' or [highway] = 'residential' or [highway] = 'living_street' or [highway] = 'unclassified' or [highway] = 'tertiary_link' or [highway] = 'residential_link' or [highway] = 'unclassified_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom16;
		&minscale_zoom17;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="5" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'tertiary' or [highway] = 'residential' or [highway] = 'living_street' or [highway] = 'unclassified' or [highway] = 'tertiary_link' or [highway] = 'residential_link' or [highway] = 'unclassified_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom18;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="6" stroke-linecap="round"/>
	</Rule>
</Style>
<!--secondary-->
<Style name="secondary">
	<Rule>
		<Filter>([highway] = 'secondary' or [highway] = 'secondary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom9;
		&minscale_zoom9;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'secondary' or [highway] = 'secondary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom10;
		&minscale_zoom10;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width=".5" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'secondary' or [highway] = 'secondary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom11;
		&minscale_zoom11;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width=".5" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'secondary' or [highway] = 'secondary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom12;
		&minscale_zoom12;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'secondary' or [highway] = 'secondary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom13;
		&minscale_zoom13;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="2" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'secondary' or [highway] = 'secondary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		&minscale_zoom15;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="4" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'secondary' or [highway] = 'secondary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom16;
		&minscale_zoom17;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="5" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'secondary' or [highway] = 'secondary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom18;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="6" stroke-linecap="round"/>
	</Rule>
</Style>
<!--primary-->
<Style name="primary">
	<Rule>
		<Filter>([highway] = 'primary' or [highway] = 'primary_link')and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom7;
		&minscale_zoom7;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width=".5" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'primary' or [highway] = 'primary_link')and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom8;
		&minscale_zoom8;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'primary' or [highway] = 'primary_link')and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom9;
		&minscale_zoom9;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'primary' or [highway] = 'primary_link')and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom10;
		&minscale_zoom11;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'primary' or [highway] = 'primary_link')and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom12;
		&minscale_zoom13;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="2" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'primary' or [highway] = 'primary_link')and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		&minscale_zoom15;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="4" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'primary' or [highway] = 'primary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom16;
		&minscale_zoom17;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="5" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'primary' or [highway] = 'primary_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom18;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="6" stroke-linecap="round"/>
	</Rule>
</Style>
<!--trunk-->
<Style name="trunk">
	<Rule>
		<Filter>([highway] = 'trunk' or [highway] = 'trunk_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom7;
		&minscale_zoom9;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'trunk' or [highway] = 'trunk_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom10;
		&minscale_zoom11;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="3"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'trunk' or [highway] = 'trunk_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom12;
		&minscale_zoom13;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="3"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'trunk' or [highway] = 'trunk_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		&minscale_zoom15;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="4"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'trunk' or [highway] = 'trunk_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom16;
		&minscale_zoom17;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="6"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'trunk' or [highway] = 'trunk_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom18;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="8"/>
	</Rule>
</Style>
<!--interchange-->
<Style name="interchange">
	<Rule>
		<Filter>([highway] = 'motorway_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom8;
		&minscale_zoom10;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom11;
		&minscale_zoom13;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		&minscale_zoom15;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="2" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom16;
		&minscale_zoom17;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="3" stroke-linecap="round"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway_link') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom18;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="5" stroke-linecap="round"/>
	</Rule>
</Style>
<!--motorway-->
<Style name="motorway">
	<Rule>
		<Filter>([highway] = 'motorway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom5;
		&minscale_zoom6;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="1"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom7;
		&minscale_zoom7;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="2"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom8;
		&minscale_zoom9;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="3"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom10;
		&minscale_zoom11;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="4"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom12;
		&minscale_zoom13;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="5"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom14;
		&minscale_zoom15;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="5"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom16;
		&minscale_zoom17;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="7"/>
	</Rule>
	<Rule>
		<Filter>([highway] = 'motorway') and not ([tunnel] = 'yes')</Filter>
		&maxscale_zoom18;
		<LineSymbolizer stroke-linejoin="round" stroke="#907d60" stroke-width="10"/>
	</Rule>
</Style>
<Layer name="pedestrian-walkways" status="off" srs="&osm2pgsql_projection;">
	<StyleName>pedestrian-walkways</StyleName>
	<Datasource>
		<Parameter name="table">
			(select way,highway,bicycle,case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel from &prefix;_line
			where highway in ('pedestrian','footway','track','path','steps')
			order by z_order,way_area desc) as roads
		</Parameter>
		&datasource-settings;
	</Datasource>
</Layer>
<Layer name="pedestrian-polygon" status="on" srs="&osm2pgsql_projection;">
	<StyleName>pedestrian-polygon</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,highway, 
	   case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel
       from &prefix;_polygon
       where highway in ('residential','unclassified','pedestrian','service','footway','living_street','track','path')
       order by z_order,way_area desc) as roads
      </Parameter>
      &datasource-settings;
	</Datasource>
</Layer>
<Layer name="cycleway" status="off" srs="&osm2pgsql_projection;">
	<StyleName>cycleway</StyleName>
	<Datasource>
		<Parameter name="table">
			(select way,highway,horse,foot,bicycle,case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel from &prefix;_line 
			where highway in ('bridleway','footway','cycleway','path')
			order by z_order) as data
		</Parameter>
		&datasource-settings;
	</Datasource>
</Layer>
<Layer name="runway" status="on" srs="&osm2pgsql_projection;">
	<StyleName>runway</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,aeroway
       from &prefix;_line
       where aeroway in ('runway','taxiway')
       order by z_order) as roads
      </Parameter>
      &datasource-settings;
	</Datasource>
</Layer>
<Layer name="rail" status="on" srs="&osm2pgsql_projection;">
	<StyleName>rail</StyleName>
	<Datasource>
		<Parameter name="table">
	      (select way,railway,
	       case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel
	       from &prefix;_line
	       where railway is not null
	       order by z_order
	      ) as roads
      </Parameter>
      &datasource-settings;
	</Datasource>
</Layer>
<Layer name="alley" status="off" srs="&osm2pgsql_projection;">
	<StyleName>alley</StyleName>
	<Datasource>
		<Parameter name="table">
	      (select way,highway,
	       case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel
	       from &prefix;_line
	       where highway = 'service'
	       order by z_order
	      ) as roads
      </Parameter>
      &datasource-settings;
	</Datasource>
</Layer>
<Layer name="tertiary" status="on" srs="&osm2pgsql_projection;">
	<StyleName>tertiary</StyleName>
	<Datasource>
		<Parameter name="table">
	      (select way,highway,
	       case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel
	       from &prefix;_line
	       where highway in ('tertiary', 'residential', 'living_street', 'unclassified', 'tertiary_link', 'residential_link', 'unclassified_link')
	       order by z_order
	      ) as roads
      </Parameter>
      &datasource-settings;
	</Datasource>
</Layer>
<Layer name="secondary" status="on" srs="&osm2pgsql_projection;">
	<StyleName>secondary</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,highway,
       case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel
       from &prefix;_roads
       where highway in ('secondary', 'secondary_link')
       order by z_order
      ) as roads
		</Parameter>
	&datasource-settings;
	</Datasource>
</Layer>
<Layer name="primary" status="on" srs="&osm2pgsql_projection;">
	<StyleName>primary</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,highway,
       case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel
       from &prefix;_roads
       where (highway in ('primary', 'primary_link'))
       order by z_order
      ) as roads
		</Parameter>
	&datasource-settings;
	</Datasource>
</Layer>
<Layer name="trunk" status="on" srs="&osm2pgsql_projection;">
	<StyleName>trunk</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,highway,
       case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel
       from &prefix;_roads
       where (highway in ('trunk', 'trunk_link'))
       order by z_order
      ) as roads
		</Parameter>
	&datasource-settings;
	</Datasource>
</Layer>
<Layer name="interchange" status="on" srs="&osm2pgsql_projection;">
	<StyleName>interchange</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,highway,
       case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel
       from &prefix;_roads
       where highway in ('motorway_link')
       order by z_order
      ) as roads
		</Parameter>
	&datasource-settings;
	</Datasource>
</Layer>
<Layer name="motorway" status="on" srs="&osm2pgsql_projection;">
	<StyleName>motorway</StyleName>
	<Datasource>
		<Parameter name="table">
      (select way,highway,
       case when tunnel in ('yes','true','1') then 'yes'::text else tunnel end as tunnel
       from &prefix;_roads
       where highway in ('motorway')
       order by z_order
      ) as roads
		</Parameter>
	&datasource-settings;
	</Datasource>
</Layer>

